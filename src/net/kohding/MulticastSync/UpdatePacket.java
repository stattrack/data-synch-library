package net.kohding.MulticastSync;

import java.io.Serializable;


public class UpdatePacket implements Serializable {
    private byte[] data = new byte[3];
    private String address;

    public UpdatePacket(byte[] d) {
        data = d;
    }

    public int getSessionID() {
        return data[0] >> 4;
    }

    public int getFieldID() {
        return data[0] & 0x0F;
    }

    public int getPayload() {
        return (data[1] << 8) + data[2];
    }

    public byte[] getData() {
        return data;
    }

    public void setAddress(String addr) {
        address = addr;
    }

    public String getAddress() {
        return address;
    }
}
