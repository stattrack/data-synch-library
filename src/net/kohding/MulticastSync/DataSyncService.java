package net.kohding.MulticastSync;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by kohbo on 9/21/2017.
 */
public class DataSyncService {
    private static final int BUFFER_SIZE = 200;


    private final ConcurrentLinkedQueue<UpdatePacket> outQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<UpdatePacket> inQueue = new ConcurrentLinkedQueue<>();
    private final Thread rcvThread = new Thread(new RcvThread());
    private final Thread sendThread = new Thread(new SendThread());
    private final Thread packetProcessorThread = new Thread(new PacketProcessor());
    private static final DataStore dataStore = new DataStore();

    private static final int multicast_port = 60055;
    private static InetAddress multicast_addr;
    private MulticastSocket multicastSocket;

    private Boolean running = false;
    private Boolean gettingSessionList = false;
    private Boolean preventLoopback = false;
    private String localAddr;
    private byte sessionID = 1;

    public DataSyncService() throws UnknownHostException {
        multicast_addr = InetAddress.getByName("239.0.0.37");
    }

    private class RcvThread implements Runnable {
        private final byte[] rcvBuffer = new byte[BUFFER_SIZE];
        private final DatagramPacket rcvPacket = new DatagramPacket(rcvBuffer, rcvBuffer.length);

        @Override
        public void run() {
//            System.out.println("RcvThread starting.");
            try {
                multicastSocket.joinGroup(multicast_addr);

                while (running) {
                    multicastSocket.receive(rcvPacket);
                    if (rcvPacket.getLength() > 0) {
                        // Deserialize the data and add it to the in queue
                        byte[] objBytes = Arrays.copyOfRange(rcvBuffer, 0, rcvPacket.getLength());
                        UpdatePacket updatePacket = new UpdatePacket(objBytes);
                        updatePacket.setAddress(rcvPacket.getAddress().getHostAddress());
                        inQueue.add(updatePacket);
                    }

                    // Clear buffer
                    for (int i = 0; i < rcvPacket.getLength(); i++) {
                        rcvBuffer[i] = 0;
                    }
                }
//                System.out.println("RcvThread stopping.");
                multicastSocket.leaveGroup(multicast_addr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class SendThread implements Runnable {
        private DatagramPacket sendPacket;

        @Override
        public void run() {
//            System.out.println("SendThread starting.");
            try {
                while (running) {
                    if (!outQueue.isEmpty()) {
//                        System.out.println("Sending update from queue.");

                        // Serialize the packet and send it
                        byte[] obj = outQueue.remove().getData();
                        sendPacket = new DatagramPacket(obj, obj.length, multicast_addr, multicast_port);
                        multicastSocket.send(sendPacket);
                    }
                }

//                System.out.println("SendThread stopping.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class PacketProcessor implements Runnable {
        @Override
        public void run() {
            while (running) {
                // check if packets waiting to be processed
                int fieldIndex = 0;
                if (!inQueue.isEmpty()) {
                    // get the next packet from the queue
                    UpdatePacket packetBeingProcessed = inQueue.remove();
                    if (packetBeingProcessed.getFieldID() == 15) {
                        dataStore.addNode(packetBeingProcessed.getAddress());
                    } else if ((byte) packetBeingProcessed.getSessionID() == sessionID) {
                        dataStore.applyUpdate(packetBeingProcessed);
                    }
                } else {
                    byte[] b = new byte[]{(byte) (0x1F + fieldIndex), (byte) 0x00, (byte) 0x00};
                    sendUpdatePacket(new UpdatePacket(b));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {

                    }
                }
            }
        }
    }

    public void startService() throws IOException {
        System.out.println("Starting service..." + multicast_addr);
        running = true;

        // Prevent going into active state from gettingSessionList state
        if (gettingSessionList) throw new IOException("Trying to start service while getting session list");

        // Open and configure multicast socket
        multicastSocket = new MulticastSocket(multicast_port);
        multicastSocket.setLoopbackMode(preventLoopback);


        // Start the worker threads
        packetProcessorThread.start();
        rcvThread.start();
        sendThread.start();
    }

    public void stopService() {
        System.out.println("Stopping service...");

        // Stop the threads
        running = false;
        try {

        } finally {
            // Make sure socket is closed
            if (!multicastSocket.isClosed() && multicastSocket != null)
                multicastSocket.close();
            multicastSocket = null;
        }
    }

    public void setInterface(InetAddress interf) {
        try {
            multicastSocket.setInterface(interf);
            localAddr = interf.getHostAddress();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public void sendUpdatePacket(UpdatePacket s) {
        outQueue.add(s);
    }

    public void clearQueues() {
        inQueue.clear();
        outQueue.clear();
    }

    public Integer[] getSessionList() throws IOException {
        HashSet<Integer> sessions = new HashSet<>();

        // Open and configure multicast socket
        multicastSocket = new MulticastSocket(multicast_port);
        multicastSocket.setLoopbackMode(true);

        long sTime = System.currentTimeMillis();
        gettingSessionList = true;
        rcvThread.start();

        while (System.currentTimeMillis() - sTime < 10000) {
            if (!inQueue.isEmpty()) {
                sessions.add(inQueue.remove().getSessionID());
            }
        }

        clearQueues();
        return (Integer[]) sessions.toArray();
    }
}
