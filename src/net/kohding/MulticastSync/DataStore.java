package net.kohding.MulticastSync;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

class DataStore {
    private static final ConcurrentHashMap<String, Integer[]> hashMap = new ConcurrentHashMap<>();
    private static String[] fields = {
            "HP",
            "MP"
    };

    public void applyUpdate(final UpdatePacket packet) {
        // Check if sending node's data is already in hashmap
        if (hashMap.containsKey(packet.getAddress())) {
            // Update the value in array
            hashMap.get(packet.getAddress())[packet.getFieldID()] = packet.getPayload();
        } else {
            Integer[] values = new Integer[fields.length];
            for (int i = 0; i < fields.length; i++) {
                values[i] = 0;
            }
            values[packet.getFieldID()] = packet.getPayload();
            hashMap.put(packet.getAddress(), values);
        }
    }

    public void addNode(String addr) {
        if (!hashMap.containsKey(addr)) {
            Integer[] values = new Integer[fields.length];
            for (int i = 0; i < fields.length; i++) {
                values[i] = 0;
            }
            hashMap.put(addr, values);
        }
    }

    public static void printHM() {
        for (String k : hashMap.keySet()) {
            System.out.println(k);
            int findex = 0;
            for (Integer i : hashMap.get(k)) {
                System.out.printf("\t%10s%10d\n", fields[findex++], i);
            }
        }
        System.out.println("");
    }

    public static void setFieldArray(String[] fieldnames) {
        fields = fieldnames;
    }

    public static String[] getFields() {
        return fields;
    }

    public static ConcurrentHashMap<String, Integer[]> getHashMap() {
        return hashMap;
    }
}