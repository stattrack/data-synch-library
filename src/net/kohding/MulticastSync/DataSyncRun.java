package net.kohding.MulticastSync;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Scanner;

public class DataSyncRun {

    public static void main(String... args) throws IOException, InterruptedException {
        System.out.println(getIp());
        DataSyncService dataSyncService = new DataSyncService();
        dataSyncService.startService();
        dataSyncService.setInterface(InetAddress.getByName(getIp()));
        if (args[0].equals("admin")) {
            Scanner in = new Scanner(System.in);
            String choice;
            do {
                System.out.print("> ");
                choice = in.nextLine();

                switch (choice) {
                    case "q":
                        dataSyncService.stopService();
                        break;

                    case "c":
                        clrScr();
                        break;

                    case "p":
                        DataStore.printHM();
                        break;

                    case "su":
                        byte[] b = new byte[]{(byte) 0x10, (byte) 0x00, (byte) 0x00};
                        System.out.print("Field ID? ");
                        b[0] = (byte) (b[0] | in.nextByte());
                        System.out.print("Payload? ");
                        b[2] = in.nextByte();
                        dataSyncService.sendUpdatePacket(new UpdatePacket(b));
                        break;
                }
            } while (!choice.equals("q"));
        } else {
            while (true) {
                System.out.println("...");
                clrScr();
                DataStore.printHM();
                Thread.sleep(1000);
            }
        }

    }

    private static String getIp() throws SocketException {
        return Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                .flatMap(i -> Collections.list(i.getInetAddresses()).stream())
                .filter(ip -> ip instanceof Inet4Address && ip.isSiteLocalAddress())
                .findFirst().orElseThrow(RuntimeException::new)
                .getHostAddress();
    }

    private static void clrScr() throws IOException {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }
        } catch (InterruptedException e) {

        }
    }
}
