@echo OFF

cls
echo Starting Wireshark container...
start docker start -ai wireshark

cls
echo Started Wireshark container
echo tshark -Y "udp.port == 60055" -e ip.src -e ip.dst -e data -T fields
echo Next: Start 1 normal containers
pause
start docker run --rm -it --name=normal1 -v C:\Users\kohbo\DevProjects\data-synch-library\out\production\MulticastDataSync:/var/src --network="datasynclib" -w="/var/src" openjdk:8-jdk-slim java net.kohding.MulticastSync.DataSyncRun 0

cls
echo Started 1 normal container
echo -> Packets show up on wireshark
echo Next: Start 6 admin containers
pause
for /l %%x in (1, 1, 6) do (
    start docker run --rm -it --name="admin%%x" -v C:\Users\kohbo\DevProjects\data-synch-library\out\production\MulticastDataSync:/var/src --network="datasynclib" -w="/var/src" openjdk:8-jdk-slim java net.kohding.MulticastSync.DataSyncRun admin
)

cls
echo Started 6 admin containers
echo -> New nodes will show up in data
echo -> Commands: p, su, q
echo Stop 1 admin container manually
echo Next: Start 1 admin container
pause
start docker run --rm -it --name=admin -v C:\Users\kohbo\DevProjects\data-synch-library\out\production\MulticastDataSync:/var/src --network="datasynclib" -w="/var/src" openjdk:8-jdk-slim java net.kohding.MulticastSync.DataSyncRun admin

cls
echo Started 1 admin container
echo Next: Start 10 admin containers
pause
for /l %%x in (7, 1, 17) do (
    start docker run --rm -it --name="admin%%x" -v C:\Users\kohbo\DevProjects\data-synch-library\out\production\MulticastDataSync:/var/src --network="datasynclib" -w="/var/src" openjdk:8-jdk-slim java net.kohding.MulticastSync.DataSyncRun admin
)

cls
echo Started 10 containers
echo Next: Stop all containers
pause
for /l %%x in (1, 1, 17) do (
	docker stop admin%%x
)
docker stop admin
docker stop normal1
docker stop wireshark